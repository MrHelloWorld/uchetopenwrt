<?php
    require 'partial/header.view.php';
?>

<h2 class="text-center"> Параметры</h2>
<form action="../settings.php" method="GET" class="border-bottom">
    <input type="hidden" name="action" value="update">
    <?php foreach ($settings as $col => $value): ?>
        <?php //$col = $param['key'];?>
        <?php //$value = $param['value']; ?>
        <div class="form-group row <?=${$col}?> mx-0">
            <div class="col-2 "></div>
            <label class="col-form-label col-3" for="<?= $col ?>"><?= Core\Alias::get($col) ?>: </label>
            <div class="col-5 ">
                <?php if($col === 'debugLevel'): ?>
                    <select class="form-control" type="text" id ="<?= $col ?>" name="<?= $col ?>">
                        <option <?= ($value=='DEBUG')? 'selected': '' ?>>DEBUG</option>
                        <option <?= ($value=='ERROR')? 'selected': '' ?>>ERROR</option>
                        <option <?= ($value=='NONE')? 'selected': '' ?>>NONE</option>
                    </select>
                <?php elseif($col === 'modbusBaudRate') :?>
                    <select class="form-control" type="text" id ="<?= $col ?>" name="<?= $col ?>">
                        <option <?= ($value=='4800')? 'selected': '' ?>>4800</option>
                        <option <?= ($value=='9600')? 'selected': '' ?>>9600</option>
                        <option <?= ($value=='19200')? 'selected': '' ?>>19200</option>
                        <option <?= ($value=='28800')? 'selected': '' ?>>28800</option>
                        <option <?= ($value=='38400')? 'selected': '' ?>>38400</option>
                        <option <?= ($value=='57600')? 'selected': '' ?>>57600</option>
                        <option <?= ($value=='115200')? 'selected': '' ?>>115200</option>
                    </select>
                <?php else:?>
                    <input class="form-control" type="text" id ="<?= $col ?>" name="<?= $col ?>" value="<?= $value ?>">
                <?php endif;?>
                    <small class="alert-danger"><?=${$col.'_msg'}?></small>
            </div>
        </div>
    <?php endforeach; ?>
    <div class="text-center">
        <button type="submit" class="btn btn-primary">Применить</button>
    </div>
    <br>
</form>

<h2 class="text-center"> Система</h2>
    <div class="text-center">
        <button type="button" data-toggle="modal" data-target="#rebootModal" class="btn btn-primary px-4" onclick='showModal("Перезагрузить?", "settings.php?action=reboot")'>Перезагрузить</button>
        <button type="button" data-toggle="modal" data-target="#rebootModal" class="btn btn-primary px-5" onclick='showModal("Сбросить к заводским установкам и стереть все данные?", "index.php?action=initDB")'>Сбросить</button>
    </div>
<br>

<?php require 'partial/footer.view.php';
