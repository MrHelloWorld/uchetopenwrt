<?php
require 'partial/header.view.php';
?>

<h3 class="text-center">Счетчики</h3>

<?php \Core\HelpTo::echoHTMLTable($countersTable, 'pm130.php', [
    'rows' => ['device_id', 'name'],
    'script' => 'pm130.php'
]) ?>

<div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4 border">
        <h5 class="text-center">Добавить:</h5>
        <form action="../pm130.php" method="get">
            <input type="hidden" class="form-control" name="action" value="add" placeholder="Modbus ID">
            <div class="form-group">
                <label>Modbus ID</label>
                <input type="text" class="form-control" name="modbus_id" value="1" placeholder="Modbus ID">
            </div>
            <div class="form-group">
                <label>Имя</label>
                <input type="text" class="form-control" name="name" value="Счетчик 1" placeholder="Имя">
            </div>
            <div class="form-group text-right">
                <button type="submit" class="btn btn-primary">Добавить</button>
            </div>
        </form>
    </div>
</div>

<br>
<?php
require 'partial/footer.view.php';

