
<nav class="navbar navbar-expand navbar-dark bg-dark">
    <a class="navbar-brand" href="index.php">УСПД</a>
    <ul class="navbar-nav collapse navbar-collapse">
        <li class="nav-item">
            <a class="nav-link <?= $_SERVER['SCRIPT_NAME']==='/pm130.php'? 'active': ''?>" href="pm130.php">Счетчики</a>
        </li>
        <li class="nav-item">
            <a class="nav-link  <?= $_SERVER['SCRIPT_NAME']==='/pm130data.php'? 'active': ''?>" href="pm130data.php">Данные</a>
        </li>
        <li class="nav-item">
            <a class="nav-link  <?= $_SERVER['SCRIPT_NAME']==='/go_go_go.php'? 'active': ''?>" href="go_go_go.php">Опросить</a>
        </li>
        <li class="nav-item">
            <a class="nav-link  <?= $_SERVER['SCRIPT_NAME']==='/log.php'? 'active': ''?>" href="log.php">Журнал</a>
        </li>
        <li class="nav-item">
            <a class="nav-link  <?= $_SERVER['SCRIPT_NAME']==='/settings.php'? 'active': ''?>" href="settings.php">Параметры</a>
        </li>
<!--        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">Параметры</a>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="modbus_map.php">Карта Modbus</a>
            </div>
        </li>-->
    </ul>
    <div class='navbar-nav'>
        <span class='nav-item nav-link active mr-5' id="time">Время УСПД: 00:00:00</span>
    </div>

    <?= $_SESSION['loggedIn']? "
    <div class='navbar-nav'>
        <a class='nav-item nav-link' href='login.php?logout'>Выйти</a>
    </div>": '';
       ?>

<script>
    jQuery(document).ready(function(){
        getTime();
        setInterval(getTime, 1000);
    });
</script>
</nav>