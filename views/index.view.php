<?php
    require 'partial/header.view.php';
?>

<h3 class="text-center"> Статус<?= count(\Core\HelpTo::getPID("go_go_go.php"))>0? ": <b> идет опрос</b>": ""?>  </h3>

<!--Response server-->
<div class="row ">
    <div class="col-2"></div>
    <div class="col-1 border-bottom border-top <?= $isDaemonActive? 'bg-success': 'bg-danger'?>"></div>
    <div class="col-5 border-bottom border-top border-right py-1 bg-light">
        Сервер
    </div>
    <div class="col-1 border-right border-top border-bottom text-center bg-light p-0">
        <a class="btn btn-primary btn-sm d-flex m-1 justify-content-center <?= $isDaemonActive == true? 'disabled': ''?>" href="index.php?action=startDaemon"> Старт</a>        
    </div>
    <div class="col-1 border-right border-top border-bottom text-center bg-light p-0">
        <a class="btn btn-primary btn-sm d-flex m-1 justify-content-center <?= $isDaemonActive == false? 'disabled': ''?>" href="index.php?action=stopDaemon"> Стоп</a>
    </div>
</div>

<!--Poll server-->
<div class="row">
    <div class="col-2 "></div>
    <div class="col-1 border-bottom <?= $pollTask->enabled? 'bg-success': 'bg-danger'?>"></div>
    <div class="col-5 border-bottom border-right py-1 bg-light ">
        Опрос счетчиков, каждые <b> <?= explode("/",$pollTask->minutes,2)[1]?> мин.</b>
    </div>
    <div class="col-1 border-right border-bottom text-center bg-light p-0">
        <a class="btn btn-primary btn-sm m-1 d-flex justify-content-center <?= $pollTask->enabled == true? 'disabled': ''?>" href="index.php?action=startPoll"> Старт</a>
    </div>
    <div class="col-1 border-right border-bottom text-center bg-light p-0">
        <a class="btn btn-primary btn-sm m-1 d-flex justify-content-center <?= $pollTask->enabled == false? 'disabled': ''?>" href="index.php?action=stopPoll"> Стоп</a>
    </div>    
</div>

<!--DB size-->
<div class="row">
    <div class="col-2"></div>
    <div class="col-1 border-bottom bg-info py-1 px-0 text-center">
        <?=
            substr(filesize(\Core\Config::getStatic('dbPath')) / 1024,0,5);
            echo ' Кб';
        ?>
    </div>
    <div class="col-5 border-bottom  border-right align-middle py-1 bg-light ">
       Размер базы данных
    </div>
    <div class="col-1 border-right border-bottom text-center bg-light p-0">
        <a class="btn btn-primary btn-sm m-1 d-flex justify-content-center" href="index.php?action=vacuum">Сжать</a>
    </div>
    <div class="col-1 border-right border-bottom text-center bg-light p-0">
        <a class="btn btn-primary btn-sm m-1 d-flex justify-content-center" href="#" onclick='showModal("Удалить все данные по счетчикам?", "index.php?action=clearDB")'>Очистить</a>        
    </div>
</div>

<!--Log size-->
<div class="row">
    <div class="col-2"></div>
    <div class="col-1 border-bottom bg-info py-2 px-0 text-center">
        <?= $logSize.' Кб';?>
    </div>
    <div class="col-5 border-bottom  border-right align-middle py-1 bg-light">
        Размер <a href="log.php">лог файла</a>
    </div>
    <div class="col-2 border-right border-bottom text-center bg-light p-0">
        <a class="btn btn-primary btn-sm d-flex m-1 justify-content-center" href="index.php?action=clearLog">Очистить</a>
    </div>
</div>

<!--Free space size-->
<div class="row">
    <div class="col-2"></div>
    <div class="col-1 border-bottom bg-info py-2 px-0 text-center">
        <?=
            printf("%5.1f", disk_free_space(\Core\Config::getStatic('toGetFreeSpace')) / 1024 /1024);
            echo ' Мб';
        ?>
    </div>
    <div class="col-5 border-bottom py-2 bg-light">
       Свободное место
    </div>
    <div class="col-2 border-right border-bottom bg-light">
<!--        <a class="btn btn-secondary btn-sm " href="index.php?action=clearLog">Очистить</a>-->
    </div>
</div>
<br>


<?php
    require 'partial/footer.view.php';

