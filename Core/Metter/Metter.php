<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Metter;

require_once 'MetterInterface.php';

use DateTime;
/**
 * Description of MetterInterface
 * 
 * @property MetterReaderInterface $metterReader
 * @property int $maxUnsync Max time diff in metter in seconds
 * 
 * @author Serg
 */
class Metter implements MetterInterface
{
    protected $metterReader;
    protected $dbWriter;
    protected $maxUnsync;

    public function __construct($metterReader, $dbWriter = null)
    {
        $this->metterReader = $metterReader;        
        $this->dbWriter = $dbWriter;
        $this->maxUnsync = 5;
    }
    
    public function setMaxUnsync($max)
    {
        $this->maxUnsync = $max;
    }

    private function checkConnection()
    {
        if (!$this->metterReader->isConnected()) {
            $this->metterReader->connect();
        }      
    }
    
    public function getTime()
    {
        $this->checkConnection();        
        return $this->metterReader->getTime();
    }

    public function setTime($hour, $min, $sec)
    {
        $this->checkConnection();
        return $this->metterReader->setTime($hour, $min, $sec);
    }

    public function getA2R2($startTime = 0)
    {
        $this->checkConnection();
        return $this->metterReader->readA2R2($startTime);
    }

    public function correctMetterTime() 
    {
        $ret = [];        
        $counterTime = $this->getTime();   
        $curTime = new DateTime();   
        $dif = $counterTime->diff($curTime);
        $total = $dif->y + $dif->m + $dif->d + $dif->h + $dif->i;
        if ($total > 0 || $dif->s > $this->maxUnsync) {

            $this->setTime($curTime->format("H"), $curTime->format("i"), $curTime->format("s"));
            $ret['oldTime'] = $counterTime->format("Y-m-d H:i:s");
            $ret['newTime'] = $curTime->format("Y-m-d H:i:s");
            return $ret;
        }
        return null;
    }

    public function close() 
    {
        $this->metterReader->disconnect();
    }

    public function open() 
    {
        $this->metterReader->connect();
    }

}
