<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Metter;

/**
 *
 * @author Serg
 */
interface MetterReaderInterface 
{
    public function connect();
    public function disconnect();
    public function isConnected();
   
    public function readA2R2($starTime = 0);
    public function getTime();
    public function setTime($hour, $min, $sec);
}
