<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Metter;

require 'ModbusTCPMetterReader.php';
require_once __DIR__.'/../Helper.php';
require_once __DIR__.'/../Logger.php';

use DateTime;
use DateTimeZone;
use Core\HelpTo;
use Core\Logger;
/**
 *
 * @author Serg
 */
 class PM130ModbusTCPReader extends ModbusTCPMetterReader
{
    CONST FILERECORD_STATUS_LAST_RECORD                 = 1;
    CONST FILERECORD_STATUS_FILE_EMPTY                  = 1 << 8;
    CONST FILERECORD_STATUS_READING_AFTER_END           = 1 << 12;
    CONST FILERECORD_STATUS_GENERIC_READ_ERROR          = 1 << 15;

    CONST ADDR_FILE_FUNCTION                            = 63120;
    CONST ADDR_FILE_ID                                  = 63120 + 1;
    CONST ADDR_SECTION_NUMBER                           = 63120+2;
    CONST ADDR_SECTION_CHANNEL_ID                       = 63120+3;    
    CONST ADDR_RECORD_SEQUENCE_NUMBER                   = 63120+4;
    CONST ADDR_FIND_KEY_START_TIME                      = 63120+8;

    CONST ADDR_DATALOG_RECORD_STATUS                    = 63160;
    CONST ADDR_DATALOG_RECORD_SEQUENCE_NUMBER           = 63160+1;
    CONST ADDR_DATALOG_RECORD_TIME                      = 63160+2;
    CONST ADDR_DATALOG_LOG_VALUE_1                      = 63160+8;
    CONST ADDR_DATALOG_LOG_VALUE_2                      = 63160+8+2;
    CONST ADDR_DATALOG_LOG_VALUE_3                      = 63160+8+4;
    CONST ADDR_DATALOG_LOG_VALUE_4                      = 63160+8+6;

    CONST ADDR_SECONDS                                  = 4352;
    CONST ADDR_MINUTES                                  = 4352+1;
    CONST ADDR_HOUR                                     = 4352+2;

    CONST FILE_FUNCTION_ACK                             = 1;
    CONST FILE_FUNCTION_SET_POSITION                    = 3;
    CONST FILE_FUNCTION_RESET_POSITION                  = 5;
    CONST FILE_FUNCTION_FIND                            = 7;
    CONST FILE_FUNCTION_READ                            = 11;
    CONST FILE_FUNCTION_ERASE                           = 127;

    CONST FILE_ID_DATA_LOG1                             = 1;

    public function __construct($modbusTCPClient)
    {
        parent::__construct($modbusTCPClient);
    }

    public function connect()
    {
        $this->modbusTCPClient->connect();
    }

    public function disconnect()
    {
        $this->modbusTCPClient->disconnect();
    }

    public function readA2R2($starTime = 0)
    {
        return $this->readDatalogFile($starTime);            
    }

    public function getTime()
    {
        $res = $this->modbusTCPClient->readHoldingRegisters(self::ADDR_SECONDS, 6);
        if (!$res->isValid())
            return null;

        $time = $res->getData('UINT16');

        $dt = new DateTime();
        //TODO: ADD DATE ROUTINE
        //$dt->setDate($time[6]+2000, $time[5], $time[4]);
        $dt->setTime($time[3], $time[2], $time[1]);
        return $dt;
    }

    public function setTime($hour, $min, $sec)
    {
        try {
            $this->modbusTCPClient->writeSingleRegister(self::ADDR_SECONDS, $sec);
            $this->modbusTCPClient->writeSingleRegister(self::ADDR_MINUTES, $min);
            $this->modbusTCPClient->writeSingleRegister(self::ADDR_HOUR, $hour);
        }
        catch (Exception $ex) {
            Logger::writeLog("Error while setTime MeterID=$this->dbID, ModbusID=$this->deviceID", 'ERROR');
            return false;
        }
        return true;
    }

    private function resetFilePosition()
    {
        $this->modbusTCPClient->writeSingleRegister(self::ADDR_FILE_ID, self::FILE_ID_DATA_LOG1);
        $this->modbusTCPClient->writeSingleRegister(self::ADDR_SECTION_NUMBER, 0);
        $this->modbusTCPClient->writeSingleRegister(self::ADDR_FILE_FUNCTION, self::FILE_FUNCTION_RESET_POSITION);
    }

    public function findData($startUnixDate)
    {
        $startUnixDate += 1;
        $this->modbusTCPClient->writeSingleRegister(self::ADDR_FILE_FUNCTION, self::FILE_FUNCTION_FIND);
        $this->modbusTCPClient->writeSingleRegister(self::ADDR_FILE_ID, self::FILE_ID_DATA_LOG1);
        $this->modbusTCPClient->writeSingleRegister(self::ADDR_SECTION_NUMBER, 0);        
        $this->modbusTCPClient->writeSingleRegister(self::ADDR_FIND_KEY_START_TIME, $startUnixDate);        
        $this->modbusTCPClient->writeSingleRegister(self::ADDR_FIND_KEY_START_TIME + 1, $startUnixDate >> 16);            
    }

    public function readDatalogFile($startUnixDate = 0)
    {
        $result = [];
        $dt = new DateTime();
        $tz = new DateTimeZone('UTC');

        $dt->setTimezone($tz);        
        if ($startUnixDate != 0) {
                $this->findData($startUnixDate);
        } else {
            $this->resetFilePosition();
        }
        $this->modbusTCPClient->writeSingleRegister(self::ADDR_FILE_FUNCTION, self::FILE_FUNCTION_READ);
        //$time -= microtime(true);
        for ($i=0; $i<15000; $i++) {
            $statusPacket = $this->modbusTCPClient->readHoldingRegisters(self::ADDR_DATALOG_RECORD_STATUS, 4);
            $status = $statusPacket->getData('UINT16')[1];
            $stamp = $statusPacket->getData("UINT32")[2];
            //echo $this->debug? "Status: $status   Stamp:  $stamp\n": "";
            if ($status & self::FILERECORD_STATUS_GENERIC_READ_ERROR) {
                Logger::writeLog("There is no data. Stop reading ModbusID={$this->modbusTCPClient->deviceID}");
                break;
            }
            if ($stamp === 0) {
                Logger::writeLog("Wrong data, stop reading ModbusID={$this->modbusTCPClient->deviceID}", 'ERROR');
                break;
            }

            $record = $this->modbusTCPClient->readHoldingRegisters(self::ADDR_DATALOG_LOG_VALUE_1, 8)->getData('UINT32'); //!!!!!!!!!MUST BE INT32
            $this->modbusTCPClient->writeSingleRegister(self::ADDR_FILE_FUNCTION, self::FILE_FUNCTION_ACK);
            $dt->setTimestamp($stamp);

            $result[$i]['value1'] = $record[1];
            $result[$i]['value2'] = $record[2];
            $result[$i]['value3'] = $record[3];
            $result[$i]['value4'] = $record[4];
            $result[$i]['string_timestamp'] = $dt->format('Y-m-d H:i:s');
            $result[$i]['unix_timestamp'] = $stamp;
            
            yield $result[$i];
            if ($status & self::FILERECORD_STATUS_LAST_RECORD) {
                $num = $i+1;
                Logger::writeLog("{$result[$i]['string_timestamp']} It was last record, cancel reading meter ModbusID={$this->modbusTCPClient->deviceID}. $num records was read.");
                //echo "{$result[$i]['string_timestamp']} It was last record, cancel reading meter ModbusID={$this->modbusTCPClient->deviceID}. $num records was read.\n";
                break;
            }
        }
        //$time += microtime(true);
        //echo "it tooks $time seconds    \n ";

        //return $result;
    }
}
