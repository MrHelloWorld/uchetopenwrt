<?php

namespace Core;

require 'Server/ResponseServerService.php';
//require 'Logger.php';
//require __DIR__.'Config.php';

//date_default_timezone_set('UTC');
set_time_limit(0);

use Core\Config;
use Core\HelpTo;
use DateTime;

/** @var ClassName $variable 
 * @property Server\AbstractService $services
 */

class Daemon
{
    /** @var ClassName $variable */
    protected $services = [];

    public function __construct()
    {
    }

    public function addService($service)
    {
        $this->services[] = $service;
    }

    public function init()
    {
        foreach ($this->services as $service) {
            $ret = $service->init();
            if (!$ret) {
                $msg = "Can not init service ".get_class($service)."\n";
                Logger::writeLog($msg, 'ERROR');
                echo $msg;
                return false;
            }
        }
        return true;
    }

    public function run()
    {
        while (true)
        {            
            foreach ($this->services as $service) {
                $service->doWork();
            }
        }
    }
}
