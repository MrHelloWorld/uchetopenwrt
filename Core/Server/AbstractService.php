<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Server;

/**
 * Description of AbstractServerService
 *
 * @author Serg
 */
abstract class AbstractService
{
    public $isReadyToRun;
    public $debug;

    protected $socket;
    
    protected $localIP;
    protected $localPort;
    protected $serverIP;
    protected $serverPort;
    
    abstract public function init();
    abstract public function doWork();
    abstract public function clean();
}
