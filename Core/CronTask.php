<?php

namespace Core;

class CronTask
{
    public $command = 'php-cli -f /www/go_go_go.php';
    public $minutes = '*';
    public $hours = '*';
    public $days = '*';
    public $months = '*';
    public $weekdays = '*';
    public $enabled = '0';

    private static function init($index)
    {           
        for ($i=0; $i<=$index; $i++) {
            exec("uci add crontabs crontab");        
            if (exec("uci get crontabs.@crontab[$index]") == 'crontab') {
                break;
            }
        }
        $cron = "uci set crontabs.@crontab[$index]";
        exec($cron . ".enabled='0'");
        exec($cron . ".minutes='2'");
        exec($cron . ".hours='*'");
        exec($cron . ".days='*'");
        exec($cron . ".months='*'");
        exec($cron . ".weekdays='*'");
        exec($cron . ".command='php-cli -f /www/go_go_go.php'");
        exec('uci commit crontabs');
        exec('/etc/init.d/cron restart');
    }

    public function set($index)
    {
        $cron = "uci get crontabs.@crontab[$index]";
        $ret = exec($cron);
        if ($ret !== 'crontab')
        {
            Logger::writeLog("Erorr getting crontab[$index]. Empty? Adding new entry...", 'ERROR');
            self::init($index);
            //exec("uci add crontabs crontab");
            //return false;
        }
        $cron = "uci set crontabs.@crontab[$index]";
        $en = $this->enabled == true? '1': '0';
        exec($cron.".enabled='$en'");
        exec($cron.".minutes='$this->minutes'");
        exec($cron.".hours='$this->hours'");
        exec($cron.".days='$this->days'");
        exec($cron.".months='$this->months'");
        exec($cron.".weekdays='$this->weekdays'");
        exec($cron.".command='$this->command'");
        exec('uci commit crontabs');
        exec('/etc/init.d/cron restart');
    }

    public static function get($index)
    {
        $cmd = "uci get crontabs.@crontab[$index]";
        $ret = exec($cmd);        
        if ($ret !== 'crontab')
        {
            Logger::writeLog("Erorr getting crontab[$index]. Empty? Adding new entry...", 'ERROR');
            self::init($index);
        }
        $cron = new CronTask();
        $cron->enabled = (exec($cmd.".enabled") == 1? true: false);
        $cron->minutes = exec($cmd.".minutes");
        $cron->hours = exec($cmd.".hours");
        $cron->days = exec($cmd.".days");
        $cron->command = exec($cmd.".command");

        return $cron;
    }
}

