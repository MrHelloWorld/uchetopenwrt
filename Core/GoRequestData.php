<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core;

require 'Config.php';
require 'Helper.php';
require 'Logger.php';
require 'Modbus/ModbusTCPClient.php';
require 'Modbus/ModbusTCPPacket.php';
require 'Metter/PM130ModbusTCPReader.php';
require 'Metter/PM130Metter.php';

use Core\Database;
use Core\DBQuery;
use Core\HelpTo;
use Core\Config;
use DateTime;
use DateInterval;
use Core\Modbus\ModbusTCPClient;
use Core\Metter\PM130ModbusTCPReader;
use Core\Metter\PM130Metter;
use Exception;
/**
 * Description of GoRequestData
 *
 * @author Serg
 */
class GoRequestData
{
    private $debug;

    public function __construct($debug = false)
    {
        $this->debug = $debug;
    }

    private function truncateDataBase()
    {
        $query = new DBQuery(Database::get());
        $dt = new DateTime();
        $dt->sub(new DateInterval('P'   .Config::get('storageDepth').'D'));
        $query->deleteLessThan('pm130_data', 'unix_timestamp', $dt->format('U'));
    }

    private function restartModbusRTUService()
    {
       if (count(HelpTo::getPID('modrelay')) < 1) {
            Logger::writeLog("Modbus gate is down! Restarting!");
            exec('/usr/bin/modrelay -t /dev/ttyS1 -a 0.0.0.0 -P 10001 -s '.Config::get('modbusBaudRate').' -p none -b 1 > /dev/null &');
            sleep(1);
        } else
        {
            exec("kill ".HelpTo::getPID("modrelay")[0]);
            exec('/usr/bin/modrelay -t /dev/ttyS1 -a 0.0.0.0 -P 10001 -s '.Config::get('modbusBaudRate').' -p none -b 1 > /dev/null &');
            sleep(1);
        }
    }

    public function init()
    {
        if (count(HelpTo::getPID('go_go_go')) > 1) {
            Logger::writeLog("Already polling data...");
            echo 'Already polling data...';
            die();
        }
        Logger::truncateLogFile();
        $this->truncateDataBase();
        $this->restartModbusRTUService();
    }

    public function run()
    {
        $query = new DBQuery(Database::get());

        $AllCounters = $query->selectAll('pm130_counters');

        $ip = Config::get('modbusGateIP');
        $port = Config::get('modbusGatePort');

        $render = "<h1 class=\"text-center\"> Данные добавлены: </h1>";

        foreach ($AllCounters as $counter) {
            $modbusId = $counter['device_id'];
            $dbID = $counter['id'];
            $numAddedLines = 0;

            $tcpClient = new ModbusTCPClient($ip, $port, $modbusId, $this->debug);
            $tcpClient->setSocketTimout(Config::get('socketTimeoutSec'));
            $pm130Reader = new PM130ModbusTCPReader($tcpClient);

            $pm130Metter = new PM130Metter($pm130Reader);
            $pm130Metter->setMaxUnsync(3);
            $pm130Metter->open();
            $lastTimeStamp = $query->selectMax('pm130_data', 'unix_timestamp', ['pm130_id'=>$dbID]);
            if ($this->debug) {
                echo "Starting requesting data from ".HelpTo::printUnixTime($lastTimeStamp)."\n";
            }
            Logger::writeLog("Start requesting: MetterID=$dbID, ModbusID={$modbusId} from: ".HelpTo::printUnixTime($lastTimeStamp));
            //echo "Request: $dbID @ $modbusId from ".HelpTo::printUnixTime($lastTimeStamp)."\n";
            
            $res = $pm130Metter->getA2R2($lastTimeStamp );

            if ($res == null) {
                //echo "NULL 1";
                Logger::writeLog("Got null from generator", 'ERROR');
                continue;
            }
            try {
                foreach ($res as $value) {
                    if ($value == null) {
                        Logger::writeLog("Got null from generator while iterating", 'ERROR');
                        //echo "NULL FROM GEN";
                        $pm130Metter->close();
                        continue;
                    }
                    $value['pm130_id'] = $dbID;
                    if ($this->debug) {
                        echo "Recieved [{$value['value1']}, {$value['value2']}, {$value['value3']}, {$value['value4']}] at ".HelpTo::printUnixTime($value['unix_timestamp'])."\n\n";
                    }
                    $query->insert("pm130_data", $value);
                    $numAddedLines++;
                }                
                $corrected = $pm130Metter->correctMetterTime();
            } catch (Exception $exc) {
                Logger::writeLog("Ошибка во время сеанса связи со счетчиком {$counter['name']}", 'ERROR');
                $pm130Metter->close();
                continue;
            }
            if ($corrected != null) {
                Logger::writeLog("Correcting metter time {$counter['name']}: from {$corrected['oldTime']} to {$corrected['newTime']}", 'ERROR');
                if ($this->debug) {
                    echo "Correcting metter time: from {$corrected['oldTime']} to {$corrected['newTime']}\n";
                }
                //echo "Correcting metter time: from {$corrected['oldTime']} to {$corrected['newTime']}\n";
            }
            $render .= "<h6 class=\"text-center\"> {$counter['name']}: <b>$numAddedLines</b> строк добавлено </h6 >";
            $render .= "<br>";
            //break;

            $pm130Metter->close();
            time_nanosleep(0, 200000000);//MODRELAY IS GOVNO i VISNET
        }
        return $render;
    }
}