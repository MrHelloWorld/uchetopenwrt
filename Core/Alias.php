<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core;

class Alias
{    
    CONST alias = [
        'listenPort' => "Локальный порт",
        'serverIP' => "IP адрес сервера",
        'serverPort' => "Порт сервера",
        'socketTimeoutSec' => "Таймаут сокета, сек",
        'debugLevel' => "Тип журнала",
        'pollTime' => "Интервал опроса, мин",  
        'storageDepth' => "Глубина хранения данных, дней",  
        'maxLogSize' => "Максимальный размер лога, КБ",  
        'id' => 'Номер',
        'recordsPerPage' => 'Данных на странице',
        'pollShift' => 'Смещение опроса, мин',
        'modbusGateIP' => 'Адрес ModbusTCP шлюза',
        'modbusGatePort' => 'Порт ModbusTCP шлюза',
        'modbusBaudRate' => 'Скорость порта Modbus, бит/с'
    ];


    public static function get($name)
    {
        //echo $name;
        if (array_key_exists($name, self::alias))
            return self::alias[$name];
        else
            return $name;
    }
    
}