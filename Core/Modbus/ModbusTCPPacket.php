<?php

namespace Core\Modbus;

require 'ModbusPacket.php';
require_once __DIR__.'/../Helper.php';

use Core\HelpTo;
    /**
     * MODBUS SEND FRAME:
     *                  Header                                  PDU             Starting Register     Number of regs to read
     * TRANSACTION_ID  PROTOCOL_ID  LENGHT  UNIT_ID        FUNCTION_CODE           START_REG           REGS_NUM
     *      2b              2b        2b       1b               1b                     2b                  2b
     *
     *
     * MODBUS RECIEVE FRAME:
     *                  Header                                  PDU
     * TRANSACTION_ID  PROTOCOL_ID  LENGHT  UNIT_ID        FUNCTION_CODE   REC_LENGHT       DATA
     *      2b              2b        2b       1b               1b            2b         REC_LENGHT
     */

class ModbusTCPPacket extends ModbusPacket
{
    public $tranatcionID;
    public $protocolID;
    public $packetLenght;
    public $recievedDataLenght;
    public $recievedData;
    protected $rawData;

public $isValid;

    public static function createFromRecieve($recievedData)
    {
        $obj = new static();

        $obj->rawData = $recievedData;
        $header = substr($recievedData, 0, 7);
        $PDU = substr($recievedData, 7, strlen($recievedData));

        $tmp = unpack("n3/cID", $header);
        $obj->tranatcionID = $tmp[1];
        $obj->protocolID = $tmp[2];
        $obj->packetLenght = $tmp[3];
        $obj->deviceID = $tmp['ID'];

        $tmp = unpack("c*", substr($PDU, 0, 2));
        $obj->functionCode = $tmp[1];
        $obj->recievedDataLenght = $tmp[2];
        $obj->recievedData = substr($PDU, 2, strlen($PDU));

        return $obj;
    }

    public static function createToReadHoldingRegisters($transID, $deviceID, $startAddr, $lenght)
    {
        $packet = parent::createToReadHoldingRegisters($deviceID, $startAddr, $lenght);
        $packet->tranatcionID = $transID;
        $packet->rawData = pack("nnn", $transID, ModbusPacket::PROTOCOL_ID, 0x06).$packet->rawData;
        return $packet;
    }

    public static function createToWriteSingleRegister($transID, $deviceID, $startAddr, $value)
    {
        $packet = parent::createToWriteSingleRegister($deviceID, $startAddr, $value);
        $packet->tranatcionID = $transID;
        $packet->rawData = pack("nnn", $transID, ModbusPacket::PROTOCOL_ID, 0x06).$packet->rawData;
        return $packet;
    }

    public function printPacket($addString = null)
    {
        HelpTo::echoStrHex('Packet', $this->packet);
        echo 'TransactionID: '.$this->tranatcionID.'<br>';
        echo 'ProtocolID: '.$this->protocolID.'<br>';
        echo 'PacketLenght: '.$this->packetLenght.'<br>';
        echo 'DeviceID: '.$this->deviceID.'<br>';
        echo 'Function Code: '.$this->functionCode.'<br>';
        echo 'Recieved Lenght: '.$this->recievedDataLenght.'<br>';
        echo 'Recieved Data: '; HelpTo::echoHex($this->recievedData);
        echo "<br>\n";
    }

    public function getData($dataType)
    {
        $ret = [0];

        switch ($dataType)
        {
            case 'UINT16':
                $format = "n*";
                $ret = unpack($format, $this->recievedData);
                break;

            case 'INT16':
                $format = "s*";
                $ret = unpack($format, $this->recievedData);
                break;

            case 'UINT32':
                $format = "n*";
                $arr = unpack($format, $this->recievedData);
                
                for ($i=0; $i<count($arr); $i+=2) {
                    $ret[] = $arr[$i+1] + $arr[$i+2] * 65536;   
                }                
                //$ret = $arr[1] + $arr[2] * 65536;                
                break;

            default:
                echo 'UNKNOW DATA FORMAT';
                break;
        }
        if (count($ret) > 2) {
            return $ret;
        }           
        return $ret[1];
    }

}
