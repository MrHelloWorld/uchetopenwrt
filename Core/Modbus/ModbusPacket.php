<?php

namespace Core\Modbus;

require_once __DIR__.'/../Helper.php';

    /**
     * MODBUS SEND FRAME:
     *                  Header                                  PDU             Starting Register     Number of regs to read
     * TRANSACTION_ID  PROTOCOL_ID  LENGHT  UNIT_ID        FUNCTION_CODE           START_REG           REGS_NUM
     *      2b              2b        2b       1b               1b                     2b                  2b
     *
     *
     * MODBUS RECIEVE FRAME:
     *                  Header                                  PDU
     * TRANSACTION_ID  PROTOCOL_ID  LENGHT  UNIT_ID        FUNCTION_CODE   REC_LENGHT       DATA
     *      2b              2b        2b       1b               1b            2b         REC_LENGHT
     */

abstract class ModbusPacket
{

    const PROTOCOL_ID                       = 0x00;
    const FNC_READ_HOLDING_REGISTERS        = 0x03;
    const FNC_WRITE_SINGLE_REGISTER         = 0x06;
    const FNC_WRITE_MULTIPLE_REGISTERS      = 0x10;

    protected $instance;

    protected $deviceID;
    protected $functionCode;
    protected $recievedData;
    protected $startAddr;
    protected $lenght;

    protected $rawData = null;

    abstract public static function createFromRecieve($recievedData);
    abstract public function getData($dataType);       
    
    public function isValid()
    {
        return  1<<8& $this->functionCode? false: true;
    }

    public static function createToReadHoldingRegisters($deviceID, $startAddr, $lenght)
    {
        $obj = new static();
        $obj->deviceID = $deviceID;
        $obj->functionCode = self::FNC_READ_HOLDING_REGISTERS;
        $obj->startAddr = $startAddr;
        $obj->lenght = $lenght;

        $obj->rawData = pack(
            "CCnn",
            $deviceID,
            self::FNC_READ_HOLDING_REGISTERS,
            $startAddr,
            $lenght
            );
        return $obj;
    }

    public static function createToWriteSingleRegister($deviceID, $startAddr, $value)
    {
        $obj = new static();
        $obj->deviceID = $deviceID;
        $obj->functionCode = self::FNC_READ_HOLDING_REGISTERS;
        $obj->startAddr = $startAddr;
        $obj->lenght = $value;

        $obj->rawData = pack(
                "CCnn",
                $deviceID,
                self::FNC_WRITE_SINGLE_REGISTER,
                $startAddr,
                $value
            );        
        return $obj;
    }
    
    /*public static function createToWriteMultipleRegisters($deviceID, $startAddr, $values)
    {
        $obj = new static();
        $obj->deviceID = $deviceID;
        $obj->functionCode = self::FNC_WRITE_MULTIPLE_REGISTERS;
        $obj->startAddr = $startAddr;
        $obj->lenght = $value;

        $obj->rawData = pack(
                "CCnn",
                $deviceID,
                self::FNC_WRITE_SINGLE_REGISTER,
                $startAddr,
                $value
            );        
        return $obj;
    }*/

    public function getRawData()
    {
        return $this->rawData;
    }
}
