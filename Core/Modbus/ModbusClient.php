<?php
namespace Core\Modbus;

abstract class ModbusClient
{
    /**
     * MODBUS FRAME:
     *
     * TRANSACTION_ID  PROTOCOL_ID  LENGHT  UNIT_ID    FUNCTION_CODE   DATA
     *      2b              2b        2b       1b            1b         (2b for start + 2bfor lenght) FOR READ_HOLDING
     */

    const PROTOCOL_ID                       = 0x00;
    const FNC_READ_HOLDING_REGISTERS        = 0x03;
    const FNC_WRITE_SINGLE_REGISTER         = 0x06;
    const FNC_WRITE_MULTIPLE_REGISTER       = 0x10;

    public $isConnected = false;
    public $deviceID;
    protected $debug;

    abstract public function connect();
    abstract public function disconnect();
    abstract public function isConnected();
    abstract public function readHoldingRegisters($startAddr, $lenght);
    abstract public function writeSingleRegister($startAddr, $value);
    //abstract public function writeMultipleRegisters($startAddr, $values, $debug = false);
}