<?php
namespace Core\Modbus;

require_once 'ModbusClient.php';
require_once __DIR__.'/../Helper.php';
require_once __DIR__.'/../Logger.php';

use Exception;
use Core\HelpTo;
use Core\Logger;

class ModbusTCPClient extends ModbusClient
{
    /**
     * MODBUS FRAME:
     *
     * TRANSACTION_ID  PROTOCOL_ID  LENGHT  UNIT_ID    FUNCTION_CODE   DATA
     *      2b              2b        2b       1b            1b         (2b for start + 2bfor lenght) FOR READ_HOLDING
     */

    protected $ip;
    protected $port;
    protected $socket = null;
    protected $socketTimeout = null;
    protected $transactionID = 1;

    public function __construct($ip, $port, $deviceID, $debug = false)
    {
        $this->ip = $ip;
        $this->port = $port;
        $this->deviceID = $deviceID;
        $this->debug = $debug;
    }

    public function setDeviceID ($id)
    {
        $this->deviceID = $id;
    }

    public function connect()
    {
        if (!$this->isConnected) {
            if ($this->debug) {
                echo "Connecting to $this->ip:$this->port... ";
            }
            $this->socket = stream_socket_client('tcp://'.$this->ip.':'.$this->port, $errno, $errstr);
            if (!$this->socket) {
                Logger::writeLog(__CLASS__.".". __FUNCTION__." $errno - $errstr");
                echo "ERROR Modbus::Open: $errno - $errstr";
                die();
                return false;
            }
            if ($this->debug) {
                echo "Done\n";
            }            
            if (!stream_set_timeout($this->socket, $this->socketTimeout) ){
                if ($this->debug) {
                    echo "Cant set timeout for socket\n";
                }            
                Logger::writeLog(__FUNCTION__." Cant set timeout for socket.");
            }
            $this->isConnected = true;
            return true;
        }
        else {
            echo "Modbus Socket is already opened\n";
        }
    }
    
    public function isConnected() 
    {
        return $this->isConnected;
    }

    public function disconnect()
    {
        if ($this->isConnected) {
            $ret = fclose($this->socket);
            $this->socket = null;
            return $ret;
        }
    }

    public function setSocketTimout($timeout)
    {
        $this->socketTimeout = $timeout;
    }   
    
    private function incTransactioID()
    {
        $this->transactionID = (($this->transactionID + 1) & 0xFF);
        $this->transactionID == 0 ? $this->transactionID++ : true;
    }

    public function readHoldingRegisters($startAddr, $lenght)
    {
        if (!$this->isConnected) {
            echo 'readHoldingRegisters: not opened';
            return null;
        }
        $packet = ModbusTCPPacket::createToReadHoldingRegisters($this->transactionID, $this->deviceID, $startAddr, $lenght);
        fwrite($this->socket, $packet->getRawData());
        //time_nanosleep(0, 50000000);
        $this->debug? HelpTo::echoStrHex("Sent    ", $packet->getRawData()): true;
        $recieved = fread($this->socket, 1024);
        if (!$recieved) {
            Logger::writeLog("Error recieving data after readHoldingRegisters($startAddr, $lenght). Recieved: $recieved");
            throw new Exception("Nothig was recieved while reading $lenght registers starting at: $startAddr");
        }
        $recPacket =  ModBusTCPPacket::createFromRecieve($recieved);
        $this->debug? HelpTo::echoStrHex("Recieved", $recPacket->getRawData()): true;

        if ($recPacket->tranatcionID !== $this->transactionID) {
            Logger::writeLog("Incorrect answer transID after readHoldingRegisters($startAddr, $lenght)");
            //throw new Exception("Incorrect answer transID after readHoldingRegisters($startAddr, $lenght)");
        }
        if (!$recPacket->isValid()) {
            Logger::writeLog("Incorrect responce function code after readHoldingRegisters($startAddr, $lenght)");
            //throw new Exception("Incorrect responce function code after readHoldingRegisters($startAddr, $lenght)");
        }
        $this->incTransactioID();
        
        return $recPacket;
    }

    public function writeSingleRegister($startAddr, $value)
    {
        if (!$this->isConnected) {
            echo 'writeHoldingRegister: not connected';
            return null;
        }
        $packet = ModbusTCPPacket::createToWriteSingleRegister($this->transactionID, $this->deviceID, $startAddr, $value);

        fwrite($this->socket, $packet->getRawData());        
        $this->debug? HelpTo::echoStrHex("Sent    ", $packet->getRawData()): true;
        
        time_nanosleep(0, 50000000);
        $recieved = fread($this->socket, 1024);
        if (!$recieved) {
            Logger::writeLog("Error recieving data after writeSingleRegister($startAddr, $value). Recieved: $recieved");
            throw new Exception("Nothig was recieved while writing $value to $startAddr");
        }
        $recPacket = ModbusTCPPacket::createFromRecieve($recieved);
        $this->debug? HelpTo::echoStrHex("Recieved", $recPacket->getRawData()): true;

        //if ($packet->tranatcionID !== $this->transactionID) {
          //  echo $this->debug? "Wrong Response TID={$this->transactionID}: ": '' ;
        //}
        if (!$recPacket->isValid()) {
            Logger::writeLog("Incorrect responce function code after writing $value to $startAddr");
            //throw new Exception("Incorrect responce function code after writing $value to $startAddr");
        }
        $this->incTransactioID();
        time_nanosleep(0, 50000000);
        return $packet;
    }
}