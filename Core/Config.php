<?php

namespace Core;

require_once 'DB.php';
require_once 'DBQuery.php';

use Core\DBQuery;
use Core\Database;

$staticCfg = [
    'dbPath' => '/root/db.db',
    'logPath' => '/root/log.txt',
    'toGetFreeSpace' => '/root',
    'timeSource' => 'NTP'
    ];

//uspd42

class Config
{
    private static $params = [];

    public static function set($param, $value)
    {
        self::get(null);
        $query = new DBQuery(Database::get());

        if ($query->selectMax('config', 'id', ['key'=>"'$param'"]) == null) {
            $query->insert('config', ['key'=>$param, 'value'=>$value]);
        } else {
            $query->sql("UPDATE config SET value='$value' WHERE key='$param'");
        }
        self::$params[$param] = $value;
    }

    public static function get($param)
    {
        if (empty(self::$params)) {
            $query = new DBQuery(Database::get());
            $res = $query->selectAll('config');
            foreach($res as $row) {
                self::$params[$row['key']] = $row['value'];
            }
        }
        return self::$params[$param];
    }
    
    public static function getAll()
    {
        self::get(null);
        return self::$params;
    }

    public static function getStatic($param)
    {
        global $staticCfg;
        return $staticCfg[$param];
    }
    
    public static function reset()
    {
        (new DBQuery(Database::get()))->sql('delete from config');

        Config::set('listenPort', '1711');
        Config::set('serverIP', '192.168.1.42');
        Config::set('serverPort', '1690');
        Config::set('socketTimeoutSec', '2');
        Config::set('modbusGateIP', '192.168.1.142');
        Config::set('modbusGatePort', '10001');
        Config::set('modbusBaudRate', '9600');
        Config::set('debugLevel', 'DEBUG');//NONE, ERROR, DEBUG
        Config::set('pollTime', '30');
        Config::set('pollShift', '3');
        Config::set('storageDepth', '30');
        Config::set('maxLogSize', '100');
        Config::set('recordsPerPage', '30');
    }
}