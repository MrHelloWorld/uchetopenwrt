<?php

require 'Core/Daemon.php';

use Core\Daemon;
use Core\HelpTo;
use Core\Server\ResponseServerService;
use Core\Config;
use Core\Logger;

set_time_limit(0);

if($argv[1] == 'stop') {
    exec("kill ".HelpTo::getPID("daemon")[0]);
    die();
}

$daemonPID = HelpTo::getPID('daemon')[0];
if (count(HelpTo::getPID('daemon')) > 1) {
    Logger::writeLog("Daemon is already running...", 'ERROR');
    echo "Daemon is already running...\n";    
    die();
}

$daemon = new Daemon();
$localIP = exec('uci get network.wan.ipaddr');
$localPort =  Config::get('listenPort'); 
$serverPort = Config::get('serverPort');        
$serverIP = Config::get('serverIP');        
        
$service = new ResponseServerService($localIP, $localPort, $serverIP, $serverPort, false);
$daemon->addService($service);
if (!$daemon->init()) {
    die('Cant init');
}
$daemon->run();
