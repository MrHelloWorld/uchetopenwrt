<?php

require 'session.php';
require 'Core/Helper.php';
require 'Core/Request.php';

use Core\Request;
use Core\Database;
use Core\DBQuery;

$query = new DBQuery(Database::get());

$act = Request::getGET('action', 'string');

if ($act == 'delete') {
    $id = Request::getGET('id', 'int');
    $query->deleteByID('pm130_counters', $id);
    $query->deleteByColumn('pm130_data', 'pm130_id', $id);
}

if ($act == 'add') {
    $modbusId = Request::getGET('modbus_id', 'int');
    $name = Request::getGET('name', 'string');

    $val = ['device_id' => $modbusId,
            'name' => $name];
    $query->insert('pm130_counters', $val);
}
if ($act == 'update') {
    $id = Request::getGET('id', 'int');
    $row = Request::getGET('row', 'string');
    $value = Request::getGET('value', 'string');    
    $query->update('pm130_counters', $id, $row, $value);
    header("Location: pm130.php");
}

$countersTable = $query->selectAll('pm130_counters');

require 'views/pm130.view.php';

