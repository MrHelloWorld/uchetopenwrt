<?php

require 'Core/Config.php';
require 'Core/Modbus/ModbusTCPClient.php';
require 'Core/Modbus/ModbusTCPPacket.php';
require 'Core/Metter/PM130ModbusTCPReader.php';
require 'Core/Metter/PM130Metter.php';
require_once 'Core/Helper.php';

use Core\Modbus\ModbusTCPClient;
use Core\Metter\PM130ModbusTCPReader;
use Core\Metter\PM130Metter;

use Core\Database;
use Core\DBQuery;
//use Core\PM130ModbusTCPClient;
use Core\HelpTo;
use Core\Config;


$query = new DBQuery(Database::get());

$AllCounters = $query->selectAll('pm130_counters');

$ip = '192.168.1.8';

//$render = "<h1 class=\"text-center\"> Данные добавлены: </h1>";
set_time_limit(1800);
foreach ($AllCounters as $counter)
{
    $modbusId = $counter['device_id'];
    $dbID = $counter['id'];
    $tcpClient = new ModbusTCPClient($ip, 10001, $modbusId, false);
    $tcpClient->setSocketTimout(Config::get('socketTimeoutSec'));

    $pm130Reader = new PM130ModbusTCPReader($tcpClient);
    $pm130Metter = new PM130Metter($pm130Reader);
    $pm130Metter->setMaxUnsync(2);

    $lastTimeStamp = $query->selectMax('pm130_data', 'unix_timestamp', ['pm130_id'=>$dbID]);

    //$pm130Metter->setTime('18', '30', '0');
    
    $res = $pm130Metter->getA2R2(0);
    if ($res == null) {
        continue;
    }

    $corrected = $pm130Metter->correctMetterTime();    
    if ($corrected != null) {
        Logger::writeLog("Correcting metter time: from {$corrected['oldTime']} to {$corrected['newTime']}", 'ERROR');
        echo "Correcting metter time: from {$corrected['oldTime']} to {$corrected['newTime']}\n";
    }

    $render .= "<h6 class=\"text-center\"> {$counter['name']}: <b>$res</b> строк добавлено </h6 >";
    $render .= "<br>";
    //break;
}
//HelpTo::render($render);
