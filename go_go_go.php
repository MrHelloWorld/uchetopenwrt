<?php

//require 'Core/Config.php';
require 'Core/GoRequestData.php';

use Core\Database;
use Core\DBQuery;
use Core\GoRequestData;
use Core\HelpTo;
use Core\Config;

set_time_limit(1500);

//ob_start();
//$cnt = ob_get_contents(); 
$go = new GoRequestData(false);
$go->init();
$render = $go->run();

//ob_clean();
HelpTo::render($render);
