## Модуль опроса счетчиков

Web интерфейс для работы в системе учета для опроса счетчиков Satec PM130 по протоколу Modbus. Ведет базу данных sqlite3 и передает запрашиваемые данные на верхний уровень системы мониторинга учета по UDP. Висит демон опроса и по настроенному интервалу собирает новые (если есть) данные со счетчиков.

Тестировалось и в текущий момент работает на роутерах IRZ RU41 под OpenWRT на дефолтном web сервере uhttpd. Заменяет дефолтную web морду роутера. Работает на предприятии в нескольких эксземплярах. Ввиду перехода на новое железо и ОС из-за дороговизны и ограниченности текущего более не поддерживается. Так сказать продемострировать заказчику, что есть решение для него.

Счетчики электроэнергии подключаются к интерфейсу RS-485.

Примерный список параметров для настройки:

    'listenPort' => "Локальный порт",
    'serverIP' => "IP адрес сервера",
    'serverPort' => "Порт сервера",
    'socketTimeoutSec' => "Таймаут сокета, сек",
    'debugLevel' => "Тип журнала",
    'pollTime' => "Интервал опроса, мин",
    'storageDepth' => "Глубина хранения данных, дней",
    'maxLogSize' => "Максимальный размер лога, КБ",
    'recordsPerPage' => 'Данных на странице',
    'pollShift' => 'Смещение опроса, мин',
    'modbusGateIP' => 'Адрес ModbusTCP шлюза',
    'modbusGatePort' => 'Порт ModbusTCP шлюза',
    'modbusBaudRate' => 'Скорость порта Modbus, бит/с'


Комментарии для установки:

opkg update
opkg install libsqlite3 php5 php5-cgi php5-cli php5-mod-pdo-sqlite zoneinfo-core  php5-mod-json php5-mod-sockets php5-mod-session
snmpd is growning in memory usage. restart? or /etc/init.d/snmpd disable
touch /root/log.txt

uhhtpd

	option script_timeout '600'
	option network_timeout '600'
	list interpreter '.php=/usr/bin/php-cgi'
	list index_page 'index.html, index.php'
	option index_file 'index.html, index.php'

/etc/init.d/uhttpd restart

set correct timezone in web

Configured sshd_config
Subsystem sftp internal-sftp

Config cron: add several cronszm
Disable SIM1 SIM2
Disable Tools/Telnet, HTTPS
Disable Services/DHCP

put run script to /etc/init.d
chmod +x  /etc/init.d/uspd
/etc/init.d/uspd enable

Set correct date.timezone in php.ini according to /usr/share/zoneinfo
max_execution_time = 1200 in php.ini

rename /www/index.html to _index.html

add symlynk log? ln -s /root/log.txt /www/log.txt

Delete time source?

index.php?action=initDB
Apply params in menu
Enable RS485 over TCP 115200

passwd


