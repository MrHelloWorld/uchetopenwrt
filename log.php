<?php

require 'session.php';
require 'Core/Logger.php';

use Core\Logger;
use Core\HelpTo;

$render = Logger::getRender();

HelpTo::render($render);
