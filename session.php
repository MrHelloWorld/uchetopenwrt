<?php

require 'Core/Config.php';

session_start();
if ($_SESSION['loggedIn'] == false)
{    
    header ("Location: login.php");
    exit();
}