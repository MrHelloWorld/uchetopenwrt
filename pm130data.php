<?php

require 'session.php';
require 'Core/Alias.php';
require 'Core/Helper.php';

use Core\DBQuery;
use Core\Database;
use Core\Config;

$query = new DBQuery(Database::get());

$allCounters = $query->selectAll('pm130_counters');


require 'views/pm130data.view.php';

