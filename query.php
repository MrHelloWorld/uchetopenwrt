<?php

require 'Core/Config.php';
require 'Core/Helper.php';
require 'Core/Request.php';

use Core\Config;
use Core\Database;
use Core\DBQuery;
use Core\Request;

$query = new DBQuery(Database::get());

$act = Request::getGET('action', 'string');

if ($act == 'getAllCounters') {
    echo json_encode($query->selectAll('pm130_counters'));
}

if ($act == 'getData') {
    $from = Request::getGET('from', 'int');
    echo json_encode($query->selectLast('pm130_data', 'unix_timestamp', $from));
}

if ($act == 'getTime') {    
    $dt = new DateTime();
    echo $dt->format("H:i:s");
    exit();
}