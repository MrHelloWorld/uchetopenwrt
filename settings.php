<?php
require 'session.php';
require 'Core/Helper.php';
require 'Core/CronTask.php';
require 'Core/Alias.php';

use Core\Database;
use Core\DBQuery;
use Core\Config;
use Core\CronTask;
use Core\HelpTo;

$action = $_GET['action'];

if ($action === 'update')
{

    foreach ($_GET as $key => $value)
    {
        if ($key === 'action') {
            continue;
        }
        $isError = false;
        
        
        switch ($key)
        {
            case 'debugLevel':
                if (!in_array($value, ['ERROR', 'DEBUG', 'NONE'])) {
                    $isError = true;
                }
                break;
                               
            case 'serverIP':
                if (!@inet_pton($value)) {
                    $isError = true;
                }            
                break;
                
            case 'modbusGateIP':
                if (!@inet_pton($value)) {
                    $isError = true;
                }            
                break;
            default :
                if (!is_numeric($value)) {
                    $isError = true;
                }                
                break;
        }
        if ($isError) {
            ${$key} = ' alert alert-danger';
            ${$key.'_msg'} = " Неверное значение: $value";
            continue;                
        }        
        Config::set($key, $value);
    }

    $task = CronTask::get(0);
    $task->minutes = Config::get('pollShift')."-59/".Config::get('pollTime');
    $task->set(0);
    //header("Location: settings.php");
    //exit();
}

if ($action === 'reboot')
{
    $what = '<h3 class="text-center">Перезагрузка...</h3>';
    HelpTo::render($what);
    exec("reboot");
    exit();
}
$settings = Config::getAll();

require 'views/settings.index.view.php';