<?php 

require 'session.php';
require 'Core/Helper.php';
require 'Core/CronTask.php';
require 'Core/Logger.php';
require 'Core/Request.php';

use Core\HelpTo;
use Core\CronTask;
use Core\Database;
use Core\DBQuery;
use Core\Config;
use Core\Logger;
use Core\Request;

$action = Request::getGET('action', 'string');

$daemonPID = HelpTo::getPID('daemon')[0];
$isDaemonActive = $daemonPID == "" ? false : true;         

if ($action == 'startDaemon') {
    Logger::writeLog("STARTING DAEMON.");
    if (!$isDaemonActive) {
        $command = "php-cli -f daemon.php > /dev/null &";
        shell_exec($command);        
    }
    header("Location: index.php");
    exit;        
}

if ($action == 'stopDaemon') {
    Logger::writeLog("STOP DAEMON.");
    $command = "kill $daemonPID";    
    exec("kill $daemonPID");
    header("Location: index.php");    
    exit;
}

if ($action == 'startPoll') {
    Logger::writeLog("STARTING POLL.");
    $pollTask = CronTask::get(0);
    $pollTask->enabled = true;
    $pollTask->set(0);
    header("Location: index.php");
    exit;        
}

if ($action == 'stopPoll') {
    Logger::writeLog("STOP POLL.");
    $pollTask = CronTask::get(0);
    $pollTask->enabled = false;
    $pollTask->set(0);
    header("Location: index.php");    
    exit;
}

if ($action == 'vacuum') {
    Logger::writeLog("VACUUM.");
    $query = new DBQuery(Database::get());
    $query->vacuum();
    header("Location: index.php");    
    exit;
}

if ($action == 'clearLog') {
    $file = fopen(Config::getStatic('logPath'), "w");
    fclose($file);
    Logger::writeLog("Log clear",'ERROR');
    header("Location: index.php");    
    exit;
}

if ($action == 'initDB') {    
    Database::get(true);
    Logger::writeLog("Init DB", 'ERROR');    
    header("Location: index.php");    
    exit;
}

if ($action == 'clearDB') {    
    $query = new DBQuery(Database::get());
    if (!$query->DBexists())
        Database::get(true);
    $query->sql("DELETE FROM pm130_data");
    Logger::writeLog("Clear DB", 'ERROR');    
    header("Location: index.php");    
    exit;
}

$pollTask = CronTask::get(0);
$logSize = Logger::getSize();
require 'views/index.view.php';